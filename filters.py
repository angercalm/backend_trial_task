import re


def title_filtering(data, value):
    return [item for item in data if len(re.findall('|'.join(value.split(' ')), item.title, re.IGNORECASE)) > 0]


def author_filtering(data, value):
    return [item for item in data if item.author == value]


def item_get(data, value):
    item = [item for item in data if item.id == value]
    return item[0] if len(item) == 1 else None