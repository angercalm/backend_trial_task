import uuid
import uvicorn
from fastapi import FastAPI, status, Header, HTTPException, Request

from config import settings
import models
import filters

fake_database = list()

app = FastAPI()


@app.post("/submit", name="submit", status_code=status.HTTP_201_CREATED)
async def submit(item: models.ItemModel, user_agent: str | None = Header(default=None)):
    data = item.dict()
    data.update({"submitted_by":user_agent})
    fake_database.append(models.SubmitItem(**data))


@app.get("/search", name="search", status_code=status.HTTP_200_OK, response_model=list[models.SearchItem])
async def search(title: str | None = None, author: str | None = None, size: int = 50):
    data = fake_database.copy()
    if title is not None:
        data = filters.title_filtering(data, title)
    if author is not None:
        data = filters.author_filtering(data, author)
    data.sort(key = lambda x: x.created_at, reverse=True)
    return data[0:size+1:1]

@app.get("/item/{guid}", name="read_item", response_model=models.SubmitItem)
async def read_item(guid: uuid.UUID):
    item = filters.item_get(fake_database, guid)
    if item:
        return item
    raise HTTPException(status_code=404, detail="Item not found.")

@app.get("/rcc", name="rcc",) #response_model=list[models.RCCItem])
async def read_rcc(request: Request):
    #print(request.url_for("read_item", guid=str(fake_database[0].id)))
    data = [
        models.RCCItem(**{"guid":str(request.url_for("read_item", guid=str(item.id))), **item.dict()})
        for item in fake_database
    ]
    return data

if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host=settings.base_host,
        port=settings.base_port,
        log_level="debug",
        reload=True
    )