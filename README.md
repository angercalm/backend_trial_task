## Requirement
- `python3.10.1`
- `fastapi`
- `uvicorn`
- `pydantic[dotenv]`
## You can start it up next step
- `python -m venv .venv && source .venv/bin/activate`
- `pip install -r requirements.txt`
- `python ./main.py`
## Tasks and timeline
- Started at `20:00` `+/- 00:15`
- [x] Initialize project `1h`
- [x] Create item models: ItemModel, SubmitItem, SearchItem, RCCItem `1h`
- [x] Add item filters `3h`
- [x] Create endpoints `1h`
- Finished at `02:00` `+/- 00:15`
