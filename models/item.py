import datetime
import uuid
import hashlib
from pydantic import BaseModel, AnyUrl, Field, validator, root_validator


class ItemModel(BaseModel):
    title: str = Field(...,)
    author: str = Field(...,)
    text: str = Field(...,)
    url: AnyUrl = Field(...,)


class SubmitItem(ItemModel):
    id: uuid.UUID = Field(default_factory=uuid.uuid4,)
    created_at: datetime.datetime = Field(default_factory=datetime.datetime.utcnow,)
    #.astimezone().isoformat
    title_md5: str|None = Field(None,)
    text_md5: str|None = Field(None,)

    title_len_ch: int|None = Field(None,)
    text_len_ch: int|None = Field(None,)

    title_len_words: int|None = Field(None,)
    text_len_words: int|None = Field(None,)

    url_hostname: str|None = Field(None,)
    url_schema: str|None = Field(None,)

    submitted_by: str = Field(...,)

    class Config:
        validate_assignment = True
        json_encoders = {
            datetime.datetime: lambda v: v.isoformat()
        }
        json_decoders = {
            datetime.datetime: lambda v: v.astimezone()
        }

    @validator("title_md5", pre=True, always=True)
    def set_title_md5(cls, value, values):
        return value or hashlib.md5(values.get("title").encode()).hexdigest()

    @validator("text_md5", pre=True, always=True)
    def set_text_md5(cls, value, values):
        return value or hashlib.md5(values.get("text").encode()).hexdigest()

    @validator("title_len_ch", pre=True, always=True)
    def set_title_len_ch(cls, value, values):
        return value or len(values.get("title"))

    @validator("text_len_ch", pre=True, always=True)
    def set_text_len_ch(cls, value, values):
        return value or len(values.get("text"))

    @validator("title_len_words", pre=True, always=True)
    def set_title_len_words(cls, value, values):
        return value or len(values.get("title").split())

    @validator("text_len_words", pre=True, always=True)
    def set_text_len_words(cls, value, values):
        return value or len(values.get("text").split())

    @validator("url_hostname", pre=True, always=True)
    def set_url_hostname(cls, value, values):
        return value or values.get("url").host

    @validator("url_schema", pre=True, always=True)
    def set_url_schema(cls, value, values):
        return value or values.get("url").scheme



class SearchItem(SubmitItem):
    class Config:
        include = {"id", "title", "author", "text", "created_at"}


class RCCItem(SubmitItem):
    guid: AnyUrl | None = Field(default=None,)

    class Config:
        allow_population_by_field_name = True
        include = {"guid", "title", "author", "url", "created_at"}
        fields = {
            "created_at": {"alias":"pubDate"},
            "url": {"alias":"link"},
            "author": {"alias":"source"},
        }