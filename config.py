from functools import lru_cache

from pydantic import BaseSettings


class Settings(BaseSettings):
    env_name: str = "local"
    base_host:str = "0.0.0.0"
    base_port:int = 9000

    class Config:
        env_file = ".env"


@lru_cache
def get_settings() -> Settings:
    settings = Settings()
    print(f"Load environment: {settings.env_name}")
    return settings

settings = get_settings()